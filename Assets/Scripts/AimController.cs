﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    public float xRange;
    
    void Start()
    {
        xRange = 3.0f;
    }
    
    void FixedUpdate()
    {
        Vector2 parentPosition = transform.parent.gameObject.transform.position;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Vector2 point = ray.origin;

        Vector2 vectDelta = point - parentPosition;

        if (vectDelta.magnitude < xRange)
            transform.localPosition = (vectDelta.normalized) * xRange;
        else
            transform.position = point;
    }
}
