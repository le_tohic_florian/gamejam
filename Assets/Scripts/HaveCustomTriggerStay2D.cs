﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HaveCustomOnTriggerStay2D
{
    void CustomOnTriggerStay2D(Collider2D collider);
}
