﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundOrchestrator : MonoBehaviour
{
    public GameObject target;
    public GameObject spriteMask;
    public int orderInLayer;
    public List<Background> backgrounds;
    public Vector2 bounds;


    // Start is called before the first frame update
    void Start()
    {
        int torderInLayer = orderInLayer;
        spriteMask.transform.localScale = new Vector3(bounds.x, bounds.y, 1);
        foreach (Background background in backgrounds)
        {
            background.initialize(torderInLayer++, bounds.y * 100, bounds.x);
        }
    }

    // Update is called once per frame
    void Update()
    {
        target = GameManager.Instance.FindWithTag("Player");
        if (target == null)
            return;

        float onTheWayPosition = Mathf.Clamp((target.transform.position.x - transform.position.x + bounds.x / 2) / bounds.x, 0, 1);

        foreach (Background background in backgrounds)
        {
            background.updateBackground(onTheWayPosition);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector2 pos = new Vector2(this.transform.position.x, this.transform.position.y);
        Vector2 upLeft = new Vector2(pos.x - (bounds.x / 2.0f), pos.y + (bounds.y / 2.0f));
        Vector2 upRight = new Vector2(pos.x + (bounds.x / 2.0f), pos.y + (bounds.y / 2.0f)); ;
        Vector2 downLeft = new Vector2(pos.x - (bounds.x / 2.0f), pos.y - (bounds.y / 2.0f)); ;
        Vector2 downRight = new Vector2(pos.x + (bounds.x / 2.0f), pos.y - (bounds.y / 2.0f)); ;

        Gizmos.DrawLine(upLeft, upRight);
        Gizmos.DrawLine(upRight, downRight);
        Gizmos.DrawLine(downRight, downLeft);
        Gizmos.DrawLine(downLeft, upLeft);
    }
}
