﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonBehaviour : MonoBehaviour
{
    public string LevelToLoad;

    public void change_scene()
    {
        SceneManager.LoadScene(LevelToLoad);
    }

    public void exit()
    {
        Application.Quit();
    }
}
