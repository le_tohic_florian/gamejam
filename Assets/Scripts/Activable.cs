﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Activable : MonoBehaviour
{
    public List<Activator> activators;
    protected bool state;


    void Update()
    {
        bool allActivatorsAreActivated = true;
        foreach (Activator activator in activators)
        {
            if (activator.IsActivated() == false)
            {
                allActivatorsAreActivated = false;
                break;
            }
        }
        if (state != allActivatorsAreActivated)
        {
            state = allActivatorsAreActivated;
            if (state)
                IsActivated();
            else
                IsDeactivated();
        }
    }

    protected abstract void IsActivated();
    protected abstract void IsDeactivated();
}
