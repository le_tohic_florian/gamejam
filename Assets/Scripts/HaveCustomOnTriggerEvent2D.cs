﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HaveCustomOnTriggerEvent2D
{
    void CustomOnTriggerEnter2D(Collider2D collider);
    void CustomOnTriggerStay2D(Collider2D collider);
}


