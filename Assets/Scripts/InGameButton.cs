﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameButton : Activator
{
    public GameObject unpushedButtonSprite;
    public GameObject pushedButtonSprite;

    private bool pushed;

    void Start()
    {
        unpushedButtonSprite.SetActive(true);
        pushedButtonSprite.SetActive(false);
        pushed = false;
	    foreach(var component in GetComponents<Component>())
	    {
 	        Debug.Log(component);
	    }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Player player = (Player)collider.GetComponent<Player>();

        if (player != null)
        {
            unpushedButtonSprite.SetActive(false);
            pushedButtonSprite.SetActive(true);
            pushed = true;
        }
    }

    public override bool IsActivated()
    {
        return (pushed);
    }
}
