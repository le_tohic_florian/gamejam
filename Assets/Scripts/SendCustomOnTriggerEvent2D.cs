﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendCustomOnTriggerEvent2D : MonoBehaviour
{
    public GameObject target;

    void OnTriggerStay2D(Collider2D collider)
    {
        HaveCustomOnTriggerEvent2D ttarget = target.GetComponent<HaveCustomOnTriggerEvent2D>();
        if (ttarget != null)
            ttarget.CustomOnTriggerStay2D(collider);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        HaveCustomOnTriggerEvent2D ttarget = target.GetComponent<HaveCustomOnTriggerEvent2D>();
        if (ttarget != null)
            ttarget.CustomOnTriggerEnter2D(collider);
    }

}
