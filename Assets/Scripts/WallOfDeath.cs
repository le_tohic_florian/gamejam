﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallOfDeath : MonoBehaviour
{
    public float speed;
    public Vector2 direction;
    
    protected Rigidbody2D rb2d;
    

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        rb2d.position = rb2d.position + direction.normalized * speed * Time.deltaTime;
    }
}
