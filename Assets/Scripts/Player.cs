﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlatformCollision
{
    public Vector2 platformPosition;
    public Vector2 impactPosition;
    public Direction direction;
    //public GameObject platformObj;

    public PlatformCollision(Vector2 _platformPosition, Vector2 _impactPosition, Direction _direction)
    {
        platformPosition = _platformPosition;
        impactPosition = _impactPosition;
        direction = _direction;
        //platformObj = _platformObj;
    }
}

public enum Direction
{
    LEFT,
    UP,
    RIGHT,
    DOWN,
    FUCK
}

public class Player : MonoBehaviour, IKillable
{
    public Scene game_over_scene;

    public bool AllowShortJump = false;
    public bool AllowFastFall = false;
    public bool AllowWallGrab = true;

    [Range(0.5f, 10.0f)]
    public float groundSpeed = 7.0f;
    [Range(0.5f, 10.0f)]
    public float airSpeed = 5.5f;
    [Range(0.5f, 10.0f)]
    public float jumpHeight = 4.0f;
    [Range(0.5f, 10.0f)]
    public float jumpLenght = 6.0f;
    [Range(0.1f, 1.0f)]
    public float shortJumpCorrection = 0.5f;
    [Range(0.5f, 10.0f)]
    public float fallLenght = 1.0f;
    public float offsetRotation = 0f;

    public bool isGrounded = false;

    private float speed = 0.0f;
    private float gravity = 0.0f;
    private bool jumping = false;
    private bool falling = true;

    private Vector2 velocity;
    private Vector2 targetVelocity;

    public bool canWallGrab = false;
    private bool wallGrab = false;

    private bool dead = false;
    public float time_after_death = 1f;

    public bool ShowGizmos = true;
    public bool ShowWires = true;
    
    private BoxCollider2D boxCollider2D;
    protected Rigidbody2D rb2d;

    public List<PlatformCollision> platformList;
    private bool platformListUpdated = true;

    private TargetPointer targetPointer;
    private Gun gun;

    private Spawner spawner;

    private Animator animator;
    public Transform tipOfTheGun;
    public Transform shoulder;
    public Transform baseOfTheGun;
    private Quaternion mouseRotation;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        targetPointer = GetComponentInChildren<TargetPointer>();
        gun = GetComponentInChildren<Gun>();
        animator = GetComponent<Animator>();
        mouseRotation = new Quaternion();

        platformList = new List<PlatformCollision>();

        velocity = new Vector2();//
        targetVelocity = new Vector2();//

        isGrounded = false;//
        
        animator.SetFloat("Speed", 0.0f);
        speed = airSpeed;//
        gravity = CalculateGravity();//
        CameraBehaviour camera = GameManager.Instance.Find("Camera").GetComponent<CameraBehaviour>();
        camera.target = this.transform;
    }


    void Update()
    {
        ParsePlatformList();

        GetInputs();
        animator.SetFloat("Speed", velocity.magnitude);
    }


    private void GetInputs()
    {
        Vector2 move = Vector2.zero;

        if (!wallGrab)
        {
            move.x = Input.GetAxis("Horizontal");
        }

        FlipCharacter(move.x);
        if (velocity.y < 0)
        {
            jumping = false;
        }
        if (AllowFastFall && !isGrounded && !falling)
        {
            if (velocity.y <= 0)
            {
                gravity = CalculateFallGravity();
                falling = true;
            }
        } // TO SEE LATER

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            isGrounded = false;
            falling = false;
            jumping = true;
            speed = airSpeed;
            velocity.y = CalculateJumpForce();
            gravity = CalculateGravity();
        }
        else if (Input.GetButtonUp("Jump") && AllowShortJump)
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * shortJumpCorrection;
            }
        }

        if (Input.GetButtonDown("Jump") && wallGrab)
        {
            wallGrab = false;
            canWallGrab = false;
            isGrounded = false;
            falling = false;
            jumping = true;
            speed = airSpeed;
            velocity.y = CalculateJumpForce();
            gravity = CalculateGravity();
        }

        if (Input.GetButtonDown("Jump") && !isGrounded && canWallGrab)
        {
            wallGrab = true;
            velocity = Vector2.zero;
        }

        targetVelocity = move * speed;
    }


    void FixedUpdate()
    {
        Ray mousePositionInScene = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 mousePositionOn2DWorld = new Vector3(mousePositionInScene.origin.x, mousePositionInScene.origin.y, 0);

        ParsePlatformList();

        mouseRotation.SetFromToRotation((tipOfTheGun.position - shoulder.position).normalized, (mousePositionOn2DWorld - shoulder.position).normalized);
        shoulder.transform.rotation = mouseRotation * shoulder.transform.rotation;
        shoulder.transform.Rotate(0f, 0.0f, offsetRotation);
        if (Input.GetMouseButton(0))
        {
            gun.PrimaryFire();
        }
        else if (Input.GetMouseButton(1))
        {
            gun.SecondaryFire(velocity);
        }
    
        if (!isGrounded && !wallGrab)
                velocity += gravity * Vector2.up * Time.deltaTime;

         velocity.x = targetVelocity.x;

         Vector2 deltaPosition = velocity * Time.deltaTime;
         Movement(deltaPosition, false);
    }


    void Movement(Vector2 move, bool yMovement)
    { 
        rb2d.MovePosition(rb2d.position + move);
    }


    private float CalculateGravity()
    {
        float gravityPower = (-2.0f * jumpHeight * (speed * speed)) / ((jumpLenght / 2.0f) * (jumpLenght / 2.0f));

        return gravityPower;
    }


    private float CalculateFallGravity()
    {
        float gravityPower = (-2.0f * jumpHeight * (speed * speed)) / ((fallLenght / 2.0f) * (fallLenght / 2.0f));

        return gravityPower;
    }


    private float CalculateJumpForce()
    {
        float jumpPower = (2.0f * jumpHeight * speed) / (jumpLenght / 2.0f);

        return jumpPower;
    }


    private void FlipCharacter(float move)
    {
        Vector3 scale = this.transform.localScale;

        if (move < 0)
        {
            this.transform.localScale = new Vector3(Mathf.Abs(scale.x) * -1f, scale.y, scale.z);
        }
        else if (move > 0)
        {
            this.transform.localScale = new Vector3(Mathf.Abs(scale.x), scale.y, scale.z);
        }
    }

    public void SetSpawner(Spawner newSpawner)
    {
	    if (spawner)
	    {
	        spawner.Disable();
	    }
	    spawner = newSpawner;
    }

    public void Kill()
    {
        if (dead)
            return;
        dead = true;
        if (spawner && spawner.IsActive())
        {
            CameraBehaviour camera = GameManager.Instance.Find("Camera").GetComponent<CameraBehaviour>();
            camera.target = spawner.transform;
            spawner.Spawn();
            
        }
        else
        {
            GameManager.Instance.EndOfTheGamu(time_after_death);
        }
        Destroy(gameObject);
    }




    public void Bump(Vector2 direction, float power)
    {
        isGrounded = false;
        velocity.x = direction.normalized.x * power;
        velocity.y = direction.normalized.y * power;
    }


    private void ParsePlatformList()
    {
        if (!platformListUpdated)
            return;

        Direction direction;

        int down = 0;
        int left = 0;
        int right = 0;
        int up = 0;
        for (int i = 0; i < platformList.Count; i++)
        {
            direction = platformList[i].direction;
            if (direction == Direction.DOWN)
                down++;
            else if (direction == Direction.LEFT)
                left++;
            else if (direction == Direction.RIGHT)
                right++;
            else if (direction == Direction.UP)
                up++;
        }

        platformList.Clear();
        platformListUpdated = false;

        if (left == 0 && right == 0)
        {
            canWallGrab = false;
        }
        else
        {
            if (AllowWallGrab)
                canWallGrab = true;
            velocity.x = 0.0f;
        }

        if (down == 0)
        {
            isGrounded = false;
        }
        else if (!jumping)
        {
            isGrounded = true;
            canWallGrab = false;
            falling = false;
             velocity.y = 0.0f;
        }

        if (up >= 1 && !falling)
        {
            falling = true;
            velocity.y = 0;
        }
    }

    private Direction GetContactDirection(Vector2 collisionNormal)
    {
        collisionNormal.Normalize();

        if (collisionNormal == Vector2.up)
        {
            return Direction.DOWN;
        }
        if (collisionNormal == Vector2.left)
        {
            return Direction.RIGHT;
        }
        if (collisionNormal == Vector2.right)
        {
            return Direction.LEFT;
        }
        if (collisionNormal == Vector2.down)
        {
            return Direction.UP;
        }

        //Debug.LogError("There is a FUCK up in the Platform collision system : " + collisionNormal.ToString());
        return Direction.FUCK;
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            platformListUpdated = true;
            Vector2 contactPosition;
            Vector2 contactNormal;
            Direction direction;
            ContactPoint2D[] contactPoints = collision.contacts;

            for (int i = 0; i < collision.contactCount; i++)
            {
                contactPosition = contactPoints[i].point;
                contactNormal = contactPoints[i].normal;
                contactNormal.x = Mathf.Round(contactNormal.x);
                contactNormal.y = Mathf.Round(contactNormal.y);
                direction = GetContactDirection(contactNormal);
                platformList.Add(new PlatformCollision(contactPosition, contactNormal, direction));
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        platformListUpdated = true;
    }


    private void OnDrawGizmos()
    {
        float raySize = 5f;

        Gizmos.color = Color.green;
        Ray mousePositionInScene = Camera.main.ScreenPointToRay(Input.mousePosition);

        Vector3 test = new Vector3(mousePositionInScene.origin.x, mousePositionInScene.origin.y, 0);
        Gizmos.DrawLine(test , baseOfTheGun.position);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(baseOfTheGun.position + (tipOfTheGun.position - baseOfTheGun.position) * 50f, baseOfTheGun.position);
        /*Gizmos.DrawLine(should.transform.position, should.transform.position + (pone.transform.position - should.transform.position).normalized * raySize );
        Gizmos.color = Color.red;
        Gizmos.DrawLine(pone.transform.position , pone.transform.position + (ptwo.transform.position - pone.transform.position).normalized * raySize);*/
    }
    

    public Vector3 V2ToV3(Vector2 v2)
    {
        return new Vector3(v2.x, v2.y, 0.0f);
    }

    public Vector2 V3ToV2(Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }
}
