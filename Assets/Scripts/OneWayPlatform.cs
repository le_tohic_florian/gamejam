﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatform : MonoBehaviour
{
    private CompositeCollider2D oneWayCollider;
    private List<Collider2D> hitColliders;
    private List<int> colliderIndexToRemove;

    // Start is called before the first frame update
    void Start()
    {
        oneWayCollider = GetComponent<CompositeCollider2D>();
        hitColliders = new List<Collider2D>();
        colliderIndexToRemove = new List<int>();
    }

    void FixedUpdate()
    {
        List<Collider2D> overlappingCollider = new List<Collider2D>();
        ContactFilter2D _noFilter = new ContactFilter2D();

        _noFilter.NoFilter();

        Physics2D.OverlapCollider(oneWayCollider, _noFilter, overlappingCollider);
        foreach (Collider2D curCollid in hitColliders)
        {
            if (!overlappingCollider.Contains(curCollid))
            {
                Physics2D.IgnoreCollision(curCollid, oneWayCollider, false);
                colliderIndexToRemove.Add(hitColliders.IndexOf(curCollid));
            }
        }
        foreach (int index in colliderIndexToRemove)
        {
            hitColliders.RemoveAt(index);
        }
        colliderIndexToRemove.Clear();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ContactPoint2D contactPoint;
        Vector2 collisionNormal;

        contactPoint = collision.GetContact(0);
        collisionNormal = contactPoint.normal;

        if (collisionNormal == Vector2.up)
        {
            hitColliders.Add(collision.collider);
            Physics2D.IgnoreCollision(collision.collider, oneWayCollider, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmo()
    {

    }
}
