﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryFlag : MonoBehaviour
{
    private AudioSource victory_audio_source;
    private bool unplayed = true;

    // Start is called before the first frame update
    void Start()
    {
        victory_audio_source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (unplayed && other.tag == "Player")
        {
            unplayed = false;
            victory_audio_source.Play();
        }
    }



}
