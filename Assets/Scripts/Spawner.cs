﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject toSpawn;
    public GameObject whereToSpawn;
    private bool active;
    private bool destroyed;
    private Animator animator;
    private float time_after_respawn = 1f;

    void Start()
    {
        destroyed = false;
        active = false;
        animator = GetComponentInChildren<Animator>();
        animator.SetBool("Active", false);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
	    if (!destroyed)
	    {
            WallOfDeath wallOfDeath = (WallOfDeath)collider.GetComponent<WallOfDeath>();

     	    if (wallOfDeath != null)
	        {
	    	    active = false;
		        destroyed = true;
                animator.SetBool("Active", false);
                return;
	        }

            Player player = (Player)collider.GetComponent<Player>();

     	    if (player != null)
	        {
		        player.SetSpawner(this);
	    	    active = true;
                animator.SetBool("Active", true);
            }
	    }
    }

    public void Spawn()
    {
        animator.SetTrigger("Spawn");
        StartCoroutine(_Spawn());
    }

    private IEnumerator _Spawn()
    {
        yield return new WaitForSeconds(time_after_respawn);
        GameObject playerObject = Instantiate(toSpawn);
        playerObject.transform.SetParent(null);
        playerObject.transform.position = whereToSpawn.transform.position;
        CameraBehaviour camera = GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>();
        camera.target = playerObject.transform;
        Player player = playerObject.GetComponent<Player>();
        player.SetSpawner(this);
    }

    public void Disable()
    {
	    active = false;
        animator.SetBool("Active", false);
    }

    public bool IsActive()
    {
        return (active);
    }
}
