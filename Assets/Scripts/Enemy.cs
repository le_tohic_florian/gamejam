﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Security.AccessControl;
using UnityEngine;

public enum EnemyState
{
    IDLE,
    PROJECTILE,
    DEAD,
    HUNTER,
    FUCK
}

public class Enemy : MonoBehaviour, IKillable
{
    // Start is called before the first frame update
    public float maxSpeed;
    public float gravity;
    public float jumpForce;
    public GameObject Anchor_left;
    public GameObject Anchor_right;
    public Material detection_material;

    public Vector2 velocity;//PRIVATE
    public Vector2 testCollisionNormal;
    public Vector2 testCollisionNormal2;
    private Vector2 oldVelocity;
    private Vector2 targetVelocity;
    //private List<Vector2> alreadyCollided;
    private int patrol_sequence_position = 0;
    private float position_to_reach;
    private bool def_goal = true;
    private float anchor_right_pos;
    private float anchor_left_pos;
    private float radius_detection = 4f;
    private Renderer enemy_renderer;
    private LayerMask layers;
    private EnemyState currentState = EnemyState.IDLE;
    private bool canKillPlayer = true;
    public float collider_radius;

    public float min_proj_behaviour = 1f;
    public float dampeningFactor = 0.5f;
    public float reflectFactor = 0.2f;
    public float maxVelocity = 50f;
    public float timeAfterDeath = 1f;
    protected const float shellRadius = 0.01f;

    protected Rigidbody2D rb2d;
    //private BoxCollider2D boxCollider2D;
    private CircleCollider2D circCol2D;

    protected ContactFilter2D contactFilter;

    public bool isGrounded = false;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();


        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;

        velocity = new Vector2();
        targetVelocity = new Vector2();
        canKillPlayer = true;

        isGrounded = false;
        position_to_reach = this.transform.position.x;
        anchor_right_pos = Anchor_right.transform.position.x;
        anchor_left_pos = Anchor_left.transform.position.x;
        enemy_renderer = GetComponent<Renderer>();
        circCol2D = GetComponent<CircleCollider2D>();
        collider_radius = circCol2D.radius;
    }

    void OnDisable()
    {
        velocity.x = 0;
        velocity.y = 0;
        isGrounded = false;
    }

    void OnEnable()
    {
        velocity.x = 0;
        velocity.y = 0;
        isGrounded = false;
        canKillPlayer = false;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {

        ContactPoint2D contactPoint;
        Vector2 collisionNormal;

        contactPoint = collision.GetContact(0);
        collisionNormal = contactPoint.normal;

        collisionNormal.x = Mathf.RoundToInt(collisionNormal.x);
        collisionNormal.y = Mathf.RoundToInt(collisionNormal.y);

        if (collision.gameObject.tag == "Platform" && collisionNormal == Vector2.up)
        {

            if (!isGrounded)
                isGrounded = isCollidingGround(collision);
            if (isGrounded && velocity.magnitude < min_proj_behaviour / 1.41)
            {
                if (currentState == EnemyState.DEAD)
                {
                    destroyEnemy();
                }
                else
                {
                    velocity.x = 0;
                    velocity.y = 0;
                    this.becomeIdle();
                }
            }
            else
            {
                isGrounded = false;
                canKillPlayer = true;
                velocity = Vector2.Reflect(velocity, collisionNormal) * dampeningFactor;
            }
        }
        else if (collision.gameObject.CompareTag("Player") && canKillPlayer)
        {
            (collision.gameObject.GetComponent<IKillable>()).Kill();
        }
        else if (collision.gameObject.CompareTag("Enemy") && (currentState == EnemyState.PROJECTILE || currentState == EnemyState.DEAD))
        {
            if (Vector2.Dot(velocity, collision.GetContact(0).normal) < 0)
            {
                Enemy hit_enemy = collision.gameObject.GetComponent<Enemy>();
                //UnityEngine.Debug.Log("<color=green><b> Velocity ennemy </b></color>" + hit_enemy.velocity.ToString());
                hit_enemy.AddImpulse(-collision.GetContact(0).normal * velocity.magnitude * dampeningFactor, Vector2.zero);
                hit_enemy.isGrounded = false;
                if (velocity.magnitude >= min_proj_behaviour / 1.41f)
                {
                    (collision.gameObject.GetComponent<IKillable>()).Kill();
                    Kill();
                }
                velocity = Vector2.zero;
                //UnityEngine.Debug.Log("<color=cyan><b> Velocity proj </b></color>" + velocity.ToString());
                //UnityEngine.Debug.Log("<color=blue><b> Velocity ennemy </b></color>" + hit_enemy.velocity.ToString());
                //UnityEngine.UnityEngine.Debug.Break();
            }
        }
    }


    void OnCollisionStay2D(Collision2D collision)
    {
        ContactPoint2D contactPoint;
        Vector2 collisionNormal;

        contactPoint = collision.GetContact(0);
        collisionNormal = contactPoint.normal;

        collisionNormal.x = Mathf.RoundToInt(collisionNormal.x);
        collisionNormal.y = Mathf.RoundToInt(collisionNormal.y);

        if (collision.gameObject.tag == "Platform")
        {
            //if (UnityEngine.UnityEngine.Debug_line)
            //{
            //    UnityEngine.UnityEngine.Debug.Log("<color=brown><b> Velocity ennemy </b></color>" + velocity.magnitude.ToString());
            //    UnityEngine.UnityEngine.Debug.Log("<color=teal><b> Test </b></color>" + (min_proj_behaviour * Mathf.Sqrt(Physics2D.gravity.magnitude) / 1.41).ToString());
            //    UnityEngine.UnityEngine.Debug.Break();
            //    UnityEngine.Debug_line = false;
            //}
            if (!isGrounded)
                isGrounded = isCollidingGround(collision);
            if (isGrounded && velocity.magnitude < min_proj_behaviour / 1.41 && Vector2.Dot(velocity, Vector2.up) < 0)
            {
                if (currentState == EnemyState.DEAD)
                {
                   destroyEnemy();
                }
                else
                {
                    velocity.x = 0;
                    velocity.y = 0;
                    this.becomeIdle();
                }
            }
            else
            {
                isGrounded = false;
                testCollisionNormal = collisionNormal;
                if (Vector2.Dot(velocity, collisionNormal) < 0)
                {
                    velocity = Vector2.Reflect(velocity, collisionNormal.normalized) * dampeningFactor;
                    canKillPlayer = true;
                }
                else if (collision.contactCount >= 2)
                {
                    contactPoint = collision.GetContact(1);
                    collisionNormal = contactPoint.normal;
                    UnityEngine.Debug.Log("Hey, je suis " + this.name + " et ma deuz normal est " + collisionNormal.ToString());
                    collisionNormal.x = Mathf.RoundToInt(collisionNormal.x);
                    collisionNormal.y = Mathf.RoundToInt(collisionNormal.y);
                    if (Vector2.Dot(velocity, collisionNormal) < 0)
                    {
                        velocity = Vector2.Reflect(velocity, collisionNormal.normalized) * dampeningFactor;
                        canKillPlayer = true;
                    }
                }
            }
        }
        else if (collision.gameObject.CompareTag("Player") && canKillPlayer)
        {
            (collision.gameObject.GetComponent<IKillable>()).Kill();
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Enemy hit_enemy = collision.gameObject.GetComponent<Enemy>();
            this.AddImpulse(collisionNormal * velocity.magnitude, Vector2.zero);
        }
    }

    private bool isCollidingGround(Collision2D collision)
    {
        int i = 0;

        List<ContactPoint2D> contactPoints = new List<ContactPoint2D>();
        Vector2 collisionNormal;
        collision.GetContacts(contactPoints);

        foreach (ContactPoint2D contact in contactPoints)
        {
            collisionNormal = contact.normal;
            collisionNormal.x = Mathf.RoundToInt(collisionNormal.x);
            collisionNormal.y = Mathf.RoundToInt(collisionNormal.y);
            if (collisionNormal == Vector2.up)
                return (true);
        }
        return (false);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
            isGrounded = false;
        else if (collision.gameObject.tag == "Player")
            canKillPlayer = true;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D[] hit_array = new RaycastHit2D[2]; ;
        ContactFilter2D filter_2d = new ContactFilter2D();

        GameObject player = GameManager.Instance.FindWithTag("Player");
        if (player == null)
            return;

        filter_2d.NoFilter();


        if (currentState == EnemyState.IDLE && Vector3.Distance(GameManager.Instance.FindWithTag("Player").transform.position, this.transform.position) <= radius_detection)
        {
            int i = 0;
            int number_hit = Physics2D.Raycast(this.transform.position, (GameManager.Instance.FindWithTag("Player").transform.position - this.transform.position).normalized, filter_2d, hit_array, Vector3.Distance(GameManager.Instance.FindWithTag("Player").transform.position, this.transform.position));
            while (i < number_hit && !(hit_array[i].collider.gameObject.tag == "Platform"))
                i = i + 1;
            if (i == number_hit)
            {
                this.becomeHunter();
            }
        }
        else if (currentState == EnemyState.HUNTER && Vector3.Distance(GameManager.Instance.FindWithTag("Player").transform.position, this.transform.position) > radius_detection)
        {
            this.becomeIdle();
        }

        enemy_move();
    }

    void enemy_move()
    {
        if (currentState == EnemyState.PROJECTILE || currentState == EnemyState.DEAD)
        {
            return;
        }
        else if (currentState == EnemyState.HUNTER)
        {
            position_to_reach = GameManager.Instance.FindWithTag("Player").transform.position.x;
        }
        else if (currentState == EnemyState.IDLE)
        {
            velocity.x = 0;
            if (patrol_sequence_position == 0)
            {
                if (def_goal)
                {
                    position_to_reach = anchor_right_pos - Random.Range(0f, 2f);
                    def_goal = false;
                }
                if (position_to_reach - this.transform.position.x > 0f && isGrounded)
                {
                    targetVelocity.x = maxSpeed;
                }
                else if (isGrounded)
                {
                    targetVelocity.x = 0;
                    def_goal = true;
                    patrol_sequence_position = (patrol_sequence_position + 1) % 2;
                }
            }
            if (patrol_sequence_position == 1)
            {
                if (def_goal)
                {
                    position_to_reach = anchor_left_pos + Random.Range(0f, 2f);
                    def_goal = false;
                }
                if (this.transform.position.x - position_to_reach > 0f && isGrounded)
                {
                    targetVelocity.x = -maxSpeed;
                }
                else if (isGrounded)
                {
                    targetVelocity.x = 0;
                    def_goal = true;
                    patrol_sequence_position = (patrol_sequence_position + 1) % 2;
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (!isGrounded)
            velocity += Physics2D.gravity * Time.deltaTime;
        else
            velocity.y = 0;
        if (velocity.magnitude >= maxVelocity)
            velocity = velocity * maxVelocity / velocity.magnitude;
        Vector2 deltaPosition = velocity * Time.deltaTime;
        Movement(rb2d.position + deltaPosition);

    }

    public void AddImpulse(Vector2 impulse, Vector2 relativeVelocity)
    {   
        velocity += impulse / rb2d.mass + relativeVelocity;
    }


    public void Movement(Vector2 move)
    {
        rb2d.MovePosition(move);
    }


    private bool DoesCollidePlatform(Collision2D collision)
    {
        Vector2 collisionNormal = collision.GetContact(0).normal;

        if (collisionNormal == Vector2.up)
            return true;

        return false;
    }

    private void destroyEnemy()
    {
        enemy_renderer.material.color = Color.red;
        velocity = Vector2.zero;
        AddImpulse(Vector2.up * 4f + Vector2.right, Vector2.zero);
        Destroy(circCol2D);
        Destroy(gameObject, timeAfterDeath);
    }

    public void Kill()
    {
        becomeDead();
    }

    public void rigidGoSleep()
    {
        rb2d.Sleep();
    }

    public void rigidWakeUp()
    {
        rb2d.WakeUp();
    }

    public void becomeProj()
    {
        currentState = EnemyState.PROJECTILE;
    }

    public void becomeIdle()
    {
        currentState = EnemyState.IDLE;
    }

    public void becomeHunter()
    {
        currentState = EnemyState.HUNTER;
    }

    public void becomeDead()
    {
        currentState = EnemyState.DEAD;
    }

    public bool isDead()
    {
        return (currentState == EnemyState.DEAD);
    }
}
