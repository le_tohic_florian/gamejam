﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour, HaveCustomOnTriggerEvent2D
{
    public float gunRadius = 10f;
    public float angleOpeningDeg = 25f;
    public int numberOfPointsOnArc = 3;
    public float tractionStrenght = 0.2f;
    public float shootStrenght = 0.2f;
    public Vector2 hidingPlace;
    public Transform baseOfTheGun;
    public Transform tipOfTheGun;

    private GameObject proj;
    private BoxCollider2D boxCollider2D;
    private PolygonCollider2D areaOfEffect;
    private float collider_radius;


    // Start is called before the first frame update
    void Start()
    {
        proj = null;
        boxCollider2D = GetComponentInChildren<BoxCollider2D>();
        areaOfEffect = GetComponentInChildren<PolygonCollider2D>();
        collider_radius = Vector2.Distance(boxCollider2D.bounds.center, boxCollider2D.bounds.max);
        areaOfEffect.SetPath(0, GetAreaPolygonPathPoints());
    }

    private List<Vector2> GetAreaPolygonPathPoints()
    {
        List<Vector2>  pathPoints = new List<Vector2>();
        Vector2 offsetPosition = new Vector2(baseOfTheGun.position.x, baseOfTheGun.position.y);
        float angleOpening = angleOpeningDeg * Mathf.Deg2Rad;
        float angleToSub = angleOpening / (numberOfPointsOnArc + 1);
        float currentAngle;

        offsetPosition = Vector2.zero;
        pathPoints.Add(offsetPosition);
        currentAngle = angleOpening / 2f;
        for (int i = 0; i <= numberOfPointsOnArc + 1; i++)
        {
            pathPoints.Add(new Vector2(gunRadius * Mathf.Sin(currentAngle), gunRadius * Mathf.Cos(currentAngle)) + offsetPosition);
            currentAngle = currentAngle - angleToSub;
        }
        return (pathPoints);
    }

    public void CustomOnTriggerStay2D(Collider2D collider)
    {
        Enemy enemy = (Enemy)collider.GetComponent<Enemy>();

        if (enemy != null && Input.GetMouseButton(0) && proj == null)
        {
            enemy.gameObject.transform.position = hidingPlace;
            enemy.gameObject.SetActive(false);
            proj = enemy.gameObject;
        }
    }

    public void CustomOnTriggerEnter2D(Collider2D collider)
    {
        Enemy enemy = (Enemy)collider.GetComponent<Enemy>();

        if (enemy != null && Input.GetMouseButton(0) && proj == null)
        {
            enemy.gameObject.transform.position = hidingPlace;
            enemy.gameObject.SetActive(false);
            proj = enemy.gameObject;
        }
    }

    public void PrimaryFire()
    {
        List<Collider2D> overlappingBodies = new List<Collider2D>();

        ContactFilter2D filter_2d = new ContactFilter2D();

        filter_2d.useDepth = true;
        filter_2d.maxDepth = 5;
        areaOfEffect.OverlapCollider(filter_2d, overlappingBodies);
        foreach (Collider2D body in overlappingBodies)
        {
            if (body.tag == "Enemy")
            {
                List<RaycastHit2D> results = new List<RaycastHit2D>();
                int number_hit = Physics2D.Raycast((Vector2)baseOfTheGun.position, ((Vector2)body.transform.position - (Vector2)baseOfTheGun.position).normalized, filter_2d, results, ((Vector2)body.transform.position - (Vector2)baseOfTheGun.position).magnitude);


                if (number_hit == 1)
                {
                    foreach (RaycastHit2D rayHit in results)
                    {
                        //Debug.Log(rayHit.rigidbody.name);
                    }

                    //Debug.Log(number_hit.ToString());
                    Enemy hit_enemy = body.GetComponent<Enemy>();
                    if (!hit_enemy.isDead())
                        hit_enemy.becomeProj();
                    hit_enemy.isGrounded = false;
                    hit_enemy.AddImpulse((boxCollider2D.transform.position - body.transform.position).normalized * tractionStrenght, Vector2.zero);
                }
            }
        }

    }

    public void SecondaryFire(Vector2 relativeVelocity)
    {

        if (proj == null)
            return;
        ContactFilter2D filter_2d = new ContactFilter2D();
        filter_2d.NoFilter();

        if (!boxCollider2D.IsTouching(filter_2d))
        {
            Enemy proj_enemy = proj.GetComponent<Enemy>();

            proj.SetActive(true);
            proj_enemy.gameObject.transform.position = (Vector2)boxCollider2D.transform.position;
            if (!proj_enemy.isDead())
                proj_enemy.becomeProj();
            proj_enemy.AddImpulse(((Vector2)tipOfTheGun.position - (Vector2)baseOfTheGun.position).normalized * shootStrenght / 1.41f, relativeVelocity);
            proj = null;
        }
    }


}
