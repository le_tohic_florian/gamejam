﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Activable
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        animator.SetBool("Open", state);
    }

    protected override void IsActivated()
    {
        animator.SetBool("Open", true);
    }

    protected override void IsDeactivated()
    {
        animator.SetBool("Open", false);
    }
}
