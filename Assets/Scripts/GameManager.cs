﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public string startScene;

    private Dictionary<string, GameObject> gameObjectFound = new Dictionary<string, GameObject>();
    private Dictionary<string, GameObject> gameObjectFoundWithTag = new Dictionary<string, GameObject>();

    protected virtual void Start ()
	{
        if (startScene != null)
		    SceneManager.LoadScene(startScene);
	}

    public GameObject Find(string name)
    {
        GameObject gameObject;
        if (gameObjectFound.TryGetValue(name, out gameObject) && gameObject != null)
            return gameObject;
        gameObjectFound[name] = GameObject.Find(name);
        return gameObjectFound[name];
    }

    public GameObject FindWithTag(string name)
    {
        GameObject gameObject;
        if (gameObjectFoundWithTag.TryGetValue(name, out gameObject) && gameObject != null)
            return gameObject;
        gameObjectFoundWithTag[name] = GameObject.FindWithTag(name);
        return gameObjectFoundWithTag[name];
    }

    public void EndOfTheGamu(float time_after_death)
    {
        StartCoroutine(WaitForGameOver(time_after_death));
    }

    private IEnumerator WaitForGameOver(float time_after_death)
    {
        yield return new WaitForSeconds(time_after_death);
        SceneManager.LoadScene("Scenes/Game_Over");
    }
}
