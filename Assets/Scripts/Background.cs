﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public GameObject emptySpritePrefab;
    public List<Sprite> sprites;
    public float offset;

    private List<GameObject> gameObjects;
    private float totalWidth;
    private float backgroundWidth;
    private float diffwidth;

    // Start is called before the first frame update
    public void initialize(int orderInLayer, float height, float width)
    {
        gameObjects = new List<GameObject>();
        backgroundWidth = 0F;
        foreach (Sprite sprite in sprites)
        {
            GameObject gameObject = Instantiate(emptySpritePrefab) as GameObject;
            gameObject.transform.parent = this.transform;
            gameObjects.Add(gameObject);
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.sortingLayerName = "backgroundLayer";
            spriteRenderer.sortingOrder = orderInLayer;
            float coefscale = height / spriteRenderer.sprite.rect.size.y;
            gameObject.transform.localScale = new Vector3(coefscale, coefscale, 1);
            backgroundWidth += spriteRenderer.sprite.rect.size.x / 100F;
            spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
        float startingpos = 0 - backgroundWidth / 2;
        foreach (GameObject gameObject in gameObjects)
        {
            float spriteWidth = gameObject.GetComponent<SpriteRenderer>().sprite.rect.size.x / 100F;
            startingpos += spriteWidth / 2;
            gameObject.transform.localPosition = new Vector2(startingpos, 0);
            startingpos += spriteWidth / 2;

        }
        totalWidth = width + 2 * offset;
        diffwidth = totalWidth - backgroundWidth;
    }

    public void updateBackground(float onTheWayPosition)
    {
        float x = onTheWayPosition * diffwidth - diffwidth / 2;
        transform.localPosition = new Vector2(x, 0);
    }
}
