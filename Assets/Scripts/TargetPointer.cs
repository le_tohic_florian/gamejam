﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPointer : MonoBehaviour
{
    public GameObject target;
    public float offsetAngle;

    private int isFlipped;

    void Start()
    {
        isFlipped = 1;
    } 

    // Update is called once per frame
    void Update()
    {
        Vector2 targetVec = target.transform.position;
        targetVec = (Vector2)transform.position - targetVec;
        float angle = (Mathf.Atan2(targetVec.y, targetVec.x) * Mathf.Rad2Deg) + offsetAngle * isFlipped;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    // maybe do a class "flippable" to get all concerned component in player
    public void flip()
    {
        isFlipped = -isFlipped;
    }

    public void flip(int dir)
    {
        isFlipped = dir / Mathf.Abs(dir);
    }
}
